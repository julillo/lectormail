package com.jlillo.lectormail.utils.csv;

import com.jlillo.lectormail.entity.BlackList;
import com.jlillo.lectormail.utils.rut.Rut;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class CsvHandler {

//    private static List<BlackListItem> list = null;
    private static List<BlackList> list = null;
    private static List<Object> customerList = null;
    private static ICsvListReader listReader = null;

    /**
     * Sets up the processors.
     *
     * @return the cell processors
     */
    private static CellProcessor[] getProcessors() {
        final CellProcessor[] processors = new CellProcessor[]{
            new NotNull(), // firstName
            new NotNull() // lastName
        };
        return processors;
    }

    /**
     *
     * @param filename
     * @return List<BlackList>
     * @throws Exception
     */
    public static List<BlackList> readWithCsvListReader(String filename) throws Exception {

        try {
            listReader = new CsvListReader(new FileReader(filename), CsvPreference.STANDARD_PREFERENCE);

            listReader.getHeader(true); // skip the header (can't be used with CsvListReader)
            final CellProcessor[] processors = getProcessors();

            list = new ArrayList<>();

            while ((customerList = listReader.read(processors)) != null) {
                list.add(new BlackList.Builder()
                        .rut(Rut.rutInt(customerList.get(0).toString()))
                        .dv(Rut.dv(customerList.get(0).toString()))
                        .observacion(customerList.get(1).toString())
                        .build());
            }

        } catch (Exception ex) {
            //do nothing
            System.out.println(String.format("Error: %s| omitiendo...", ex.toString()));
        } finally {
            if (listReader != null) {
                listReader.close();
            }
        }
        return list;
    }
}

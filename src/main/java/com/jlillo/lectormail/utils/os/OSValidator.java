package com.jlillo.lectormail.utils.os;

/**
 * OSValidator
 *
 * @since 2009
 * @author Original code by mkyong | May 4, 2009 | Updated : November 5, 2012
 * @see
 * https://www.mkyong.com/java/how-to-detect-os-in-java-systemgetpropertyosname/
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class OSValidator {

    /**
     * MICROSOFT/WINDOWS OS
     */
    public static final int WINDOWS = 1;
    /**
     * APPLE/MACINTOSH OS
     */
    public static final int MAC = 2;
    /**
     * UNIX/LINUX OS
     */
    public static final int UNIX = 3;
    /**
     * SUN/SOLARIS OS
     */
    public static final int SOLARIS = 4;
    /**
     * OTHER
     */
    public static final int OTHER = 99;

    /**
     * OS FULL NAME
     */
    public static final String OS = System.getProperty("os.name").toLowerCase();

    /**
     * switch acordingly OS
     *
     * @return int
     */
    public static int switchOS() {
        if (isWindows()) {
            return WINDOWS;
        } else if (isMac()) {
            return MAC;
        } else if (isUnix()) {
            return UNIX;
        } else if (isSolaris()) {
            return SOLARIS;
        } else {
            return OTHER;
        }
    }

    /**
     * isWindows
     *
     * @return <b>TRUE</b> if Windows system, <b>FALSE</B> otherwise.
     */
    public static boolean isWindows() {

        return (OS.contains("win"));

    }

    /**
     * isWindows
     *
     * @return <b>TRUE</b> if Macintosh system, <b>FALSE</B> otherwise.
     */
    public static boolean isMac() {

        return (OS.contains("mac"));

    }

    /**
     * isWindows
     *
     * @return <b>TRUE</b> if UNIX/Linux system, <b>FALSE</B> otherwise.
     */
    public static boolean isUnix() {

        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);

    }

    /**
     * isWindows
     *
     * @return <b>TRUE</b> if SOLARIS system, <b>FALSE</B> otherwise.
     */
    public static boolean isSolaris() {

        return (OS.contains("sunos"));

    }
}

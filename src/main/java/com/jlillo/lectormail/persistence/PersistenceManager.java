package com.jlillo.lectormail.persistence;

import com.jlillo.lectormail.config.Config;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public enum PersistenceManager {
    INSTANCE;

    private EntityManagerFactory emFactory;
    private Map<String, String> persistenceMap;

    private PersistenceManager() {
        persistenceMap = new HashMap<String, String>();
        persistenceMap.put("javax.persistence.jdbc.url", Config.STORE_BD_HOST);
        persistenceMap.put("javax.persistence.jdbc.user", Config.STORE_BD_USER);
        persistenceMap.put("javax.persistence.jdbc.password", Config.STORE_BD_PASSWORD);
        persistenceMap.put("javax.persistence.jdbc.driver", Config.STORE_BD_DRIVER);
    }

    public EntityManager getEntityManager() {
        emFactory = Persistence.createEntityManagerFactory("lectormail-unit", persistenceMap);
        return emFactory.createEntityManager();
    }

    public void close() {
        emFactory.close();
    }
}

package com.jlillo.lectormail.controllers;

//<editor-fold defaultstate="collapsed" desc="imports">
import com.google.gson.Gson;
import com.jlillo.lectormail.config.Config;
import com.jlillo.lectormail.entity.BlackList;
import com.jlillo.lectormail.entity.EmailLog;
import com.jlillo.lectormail.persistence.PersistenceManager;
import com.jlillo.lectormail.utils.csv.CsvHandler;
import com.jlillo.lectormail.utils.mail.Mail;
import com.jlillo.lectormail.utils.os.OSValidator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
//</editor-fold>

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class LectorMailController {

//<editor-fold defaultstate="collapsed" desc="private params">
    private Mail.Builder mailBuilder;
    private EntityManager em; 
    private Mail mail;
    private Folder emailFolder;
    private Message[] messages;
    private String tempfile;
    private Multipart multipart;
    private BodyPart bodyPart;
    private InputStream is;
    private String ext1;
    private File f;
    private FileOutputStream fos;
    private byte[] buf;
    private int bytesRead;
    private List<BlackList> list;
//</editor-fold>

    public LectorMailController() {
         mailBuilder = new Mail.Builder();
    }

    public void check(String user, String password) {
        em = PersistenceManager.INSTANCE.getEntityManager();

        try {
//<editor-fold defaultstate="collapsed" desc="starting pop3 connection...">
            this.mail = mailBuilder
                    .host(Config.MAIL_IN_HOST)
                    .storeType(Config.MAIL_IN_TYPE)
                    .storePort(Config.MAIL_IN_PORT)
                    .user(user)
                    .password(password)
                    .build();
            mail.connect();
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="access to INBOX and reading data...">
            this.readInbox();
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="closing pop3 connection...">
            //close the store and folder objects
            emailFolder.close(false);
            mail.close();
//</editor-fold>
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(LectorMailController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(LectorMailController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LectorMailController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LectorMailController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            em.close();
            PersistenceManager.INSTANCE.close();
            System.out.println("Exiting, have a nice day... :)");
        }
    }

    public void readInbox() throws MessagingException, FileNotFoundException, IOException, Exception {

//<editor-fold defaultstate="collapsed" desc="create the folder object and retrieve the messages ...">
        //create the folder object and open it
        emailFolder = mail.getStore().getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);

        // retrieve the messages from the folder in an array and print it
        messages = emailFolder.getMessages();
        System.out.println(String.format("Mensajes para procesar: %d", messages.length));
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="retrieve unread messages (IMAP) ...">
//        Message messages[] = emailFolder.search(new FlagTerm(new Flags(
//                Flags.Flag.SEEN), false));
//        System.out.println(String.format("No. of Unread Messages : %d", messages.length));
//</editor-fold>
        for (int i = 0, n = messages.length; i < n; i++) {
//<editor-fold defaultstate="collapsed" desc="iterates on every email...">

            Message message = messages[i];
            try {
//<editor-fold defaultstate="collapsed" desc="create mail entity...">
                EmailLog email = new EmailLog.Builder()
                        .from(new Gson().toJson(message.getFrom()))
                        .to(new Gson().toJson(message.getAllRecipients()))
                        .subject(message.getSubject())
                        .attachments(message.getContent().toString())
                        .receivedAt(message.getSentDate())
                        .build();
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="get mails from bd...">
                TypedQuery<EmailLog> query
                        = em.createQuery("SELECT e FROM EmailLog e "
                                + "where "
                                + "e.from like :from "
                                + "and e.subject like :subject "
                                + "and e.receivedDate = :receivedDate ",
                                EmailLog.class);
                query.setParameter("from", email.getFrom());
                query.setParameter("subject", email.getSubject());
                query.setParameter("receivedDate", email.getReceivedDate(), TemporalType.TIMESTAMP);
                List<EmailLog> results = query.getResultList();
//</editor-fold>
                if (results.isEmpty()) {
//<editor-fold defaultstate="collapsed" desc="if not exists, it's saved so it's omitted next time...">
                    em.getTransaction().begin();
                    em.persist(email);
                    em.getTransaction().commit();
//</editor-fold>
                    processAttachments(message);
                } else {
                    System.out.println(String.format("Error: Ya leído | Email: %s | omitiendo...", email.toString()));
                }
            } catch (Exception ex) {
                em.getTransaction().rollback();
                System.out.println(String.format("Error: %s | omitiendo...", ex.toString()));
            }
//</editor-fold>
        }
    }

    /**
     * Reads, process and upload the data
     *
     * @param message
     * @throws IOException
     * @throws MessagingException
     * @throws Exception
     */
    public void processAttachments(Message message) throws IOException, MessagingException, Exception {
        multipart = (Multipart) message.getContent();

        for (int ii = 0; ii < multipart.getCount(); ii++) {
            bodyPart = multipart.getBodyPart(ii);
            if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())
                    && !StringUtils.isNotBlank(bodyPart.getFileName())) {
                continue; // dealing with attachments only
            }
            System.out.println(String.format("Attachment: %s", bodyPart.getFileName()));

            downloadTemps();
            uploadToDB();

        }
    }

    private void downloadTemps() throws IOException, MessagingException {
        is = bodyPart.getInputStream();

        switch (OSValidator.switchOS()) {
            case OSValidator.WINDOWS:
                tempfile = String.format("C:\\tmp\\%s", bodyPart.getFileName());
                break;
            case OSValidator.OTHER:
                throw new UnsupportedOperationException();
            default:
                tempfile = String.format("/tmp/%s", bodyPart.getFileName());
        }

        ext1 = FilenameUtils.getExtension(tempfile);

        System.out.println(String.format("Temp File: %s", tempfile));
        f = new File(tempfile);
        fos = new FileOutputStream(f);
        buf = new byte[4096];

        while ((bytesRead = is.read(buf)) != -1) {
            fos.write(buf, 0, bytesRead);
        }
        fos.close();
    }

    /**
     * upload data to BD
     *
     * @throws Exception
     */
    private void uploadToDB() throws Exception {
        switch (ext1) {
            case "csv":
                list = CsvHandler.readWithCsvListReader(tempfile);
                list.forEach((item) -> {
                    try {
                        TypedQuery<BlackList> query
                                = em.createQuery(String.format("SELECT b FROM BlackList b where b.rut like '%s' and b.observacion like '%s'", item.getRut(), item.getObservacion()), BlackList.class);
                        List<BlackList> results = query.getResultList();
                        if (results.isEmpty()) {
                            em.getTransaction().begin();
                            em.persist(item);
                            em.getTransaction().commit();
                        } else {
                            System.out.println(String.format("Error: registro duplicado | Blacklist: %s |omitiendo...", item.toString()));
                        }
                    } catch (Exception ex) {
                        em.getTransaction().rollback();
                        System.out.println(String.format("Error: %s | omitiendo...", ex.toString()));
                    }
                });
                break;
            default:
                System.out.println(String.format("Error: No es lo que busco [%s] | omitiendo...", ext1));
        }
    }
}

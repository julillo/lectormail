/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jlillo.lectormail.config;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 *
 * @author desarrollador
 */
public class Config {

    public static String MAIL_IN_HOST = "mail.google.com";
    public static int MAIL_IN_PORT = 993;
    public static String MAIL_IN_TYPE = "IMAP";
    public static String MAIL_OUT_HOST = "mail.google.com";
    public static int MAIL_OUT_PORT = 465;
    public static String MAIL_OUT_TYPE = "SMTP";

    public static String STORE_BD_HOST = "jdbc:mysql://localhost:3306/easy";
    public static String STORE_BD_USER = "someuser";
    public static String STORE_BD_PASSWORD = "somepassword";
    public static String STORE_BD_DRIVER = "com.mysql.jdbc.Driver";

    private static final Option PARAM_MAIL_IN_HOST = new Option("mih", "mail-in-host", true, "El hostname de entrada (mail).");
    private static final Option PARAM_MAIL_IN_PORT = new Option("mip", "mail-in-port", true, "El puerto de entrada (mail).");
    private static final Option PARAM_MAIL_IN_TYPE = new Option("mit", "mail-in-type", true, "El tipo de entrada (mail).");
    private static final Option PARAM_MAIL_USER = new Option("mu", "mail-user", true, "User (mail).");
    private static final Option PARAM_MAIL_PASS = new Option("mp", "mail-user-password", true, "Password (mail).");
    private static final Option PARAM_DATA_BASE_URL = new Option("dbh", "data-base-url", true, "URL base de datos (target).");
    private static final Option PARAM_DATA_BASE_USER = new Option("dbu", "data-base-user", true, "Usuario base de datos (target).");
    private static final Option PARAM_DATA_BASE_PASS = new Option("dbp", "data-base-user-pass", true, "Password base de datos (target).");
    private static final Option PARAM_DATA_BASE_DRIVER = new Option("dbd", "data-base-driver", true, "Driver base de datos (target).");

    private static final Option PARAM_HELP = new Option("help", "print help message");

    public static Options OPCTIONS = new Options()
            .addOption(PARAM_MAIL_IN_HOST)
            .addOption(PARAM_MAIL_IN_PORT)
            .addOption(PARAM_MAIL_IN_TYPE)
            .addOption(PARAM_MAIL_USER)
            .addOption(PARAM_MAIL_PASS)
            .addOption(PARAM_DATA_BASE_URL)
            .addOption(PARAM_DATA_BASE_USER)
            .addOption(PARAM_DATA_BASE_PASS)
            .addOption(PARAM_DATA_BASE_DRIVER)
            .addOption(PARAM_HELP);

}

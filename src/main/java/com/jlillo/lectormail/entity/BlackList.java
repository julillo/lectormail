package com.jlillo.lectormail.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * Juan Lillo <ju.lillo.rojas@gmail.com>
 *
 * @author desarrollador
 */
@Entity(name = "BlackList")
@Table(name = "blacklist_rut")
public class BlackList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID", unique = true, nullable = false)
    private int id;

    @Column(name = "RUT", unique = false, nullable = false)
    private int rut;

    @Column(name = "DV", length = 1, unique = false, nullable = false)
    private String dv;

    @Column(name = "NOMBRE", length = 200, unique = false, nullable = true)
    private String nombre;

    @Column(name = "OBSERVACION", length = 300, unique = false, nullable = true)
    private String observacion;

    @Column(name = "ACTIVO", length = 1)
    private String estado;

    @Column(name = "CREATED_AT", unique = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public static class Builder {

        private int _id;

        private int _rut;

        private String _dv;

        private String _nombre = null;

        private String _observacion = null;

        private String _estado = "1";

        private Date _created_at = new Date();

        public Builder id(int _id) {
            this._id = _id;
            return this;
        }

        public Builder rut(int _rut) {
            this._rut = _rut;
            return this;
        }

        public Builder dv(String _dv) {
            this._dv = _dv;
            return this;
        }

        public Builder nombre(String _nombre) {
            this._nombre = _nombre;
            return this;
        }

        public Builder observacion(String _observacion) {
            this._observacion = _observacion;
            return this;
        }

        public Builder estado(String _estado) {
            this._estado = _estado;
            return this;
        }

        public Builder createdAt(Date _created_at) {
            this._created_at = _created_at;
            return this;
        }

        public BlackList build() {
            return new BlackList(this);
        }

    }

    public BlackList() {
    }

    private BlackList(Builder b) {
        this.id = b._id;
        this.rut = b._rut;
        this.dv = b._dv;
        this.nombre = b._nombre;
        this.observacion = b._observacion;
        this.estado = b._estado;
        this.createdAt = b._created_at;
    }

}

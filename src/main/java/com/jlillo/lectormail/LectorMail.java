package com.jlillo.lectormail;

import com.jlillo.lectormail.config.Config;
import com.jlillo.lectormail.controllers.LectorMailController;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 * @since 0.0.1-SNAPSHOT, 2017.
 */
public class LectorMail {

    public static void main(String[] args) {

//<editor-fold defaultstate="collapsed" desc="default config...">
        //change accordingly
        Config.MAIL_IN_HOST = "email.expert-choice.com";
        Config.MAIL_IN_PORT = 110;
        Config.MAIL_IN_TYPE = "pop3s";
        Config.STORE_BD_HOST = "jdbc:mysql://localhost:3307/qaeasy?serverTimezone=America/Santiago";
        Config.STORE_BD_USER = "root";
        Config.STORE_BD_PASSWORD = "";
        Config.STORE_BD_DRIVER = "com.mysql.jdbc.Driver";
        //change accordingly, stored localy for security
        String username = "data@expert-choice.com";
        String password = "45_%cFGh!as";
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="apache commandline cli config">
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(Config.OPCTIONS, args);
            if (line.hasOption("mail-in-host")) {
                Config.MAIL_IN_HOST = line.getOptionValue("mail-in-host");
            }
            if (line.hasOption("mail-in-port")) {
                Config.MAIL_IN_PORT = Integer.parseInt(line.getOptionValue("mail-in-port"));
            }
            if (line.hasOption("mail-in-type")) {
                Config.MAIL_IN_TYPE = line.getOptionValue("mail-in-type");
            }
            if (line.hasOption("mail-user")) {
                username = line.getOptionValue("mail-user");
            }
            if (line.hasOption("mail-user-password")) {
                password = line.getOptionValue("mail-user-password");
            }
            if (line.hasOption("data-base-url")) {
                Config.STORE_BD_HOST = line.getOptionValue("data-base-url");
            }
            if (line.hasOption("data-base-user")) {
                Config.STORE_BD_USER = line.getOptionValue("data-base-user");
            }
            if (line.hasOption("data-base-user-pass")) {
                Config.STORE_BD_PASSWORD = line.getOptionValue("data-base-user-pass");
            }
            if (line.hasOption("data-base-driver")) {
                Config.STORE_BD_DRIVER = line.getOptionValue("data-base-driver");
            }
            if (line.hasOption("help")) {
                // automatically generate the help statement
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar LectorMail.jar", Config.OPCTIONS, true);
                System.exit(0);
            }
        } catch (Exception exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }
//</editor-fold>

        LectorMailController lmc = new LectorMailController();
        lmc.check(username, password);

    }

}
